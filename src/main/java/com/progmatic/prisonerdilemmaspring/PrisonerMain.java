/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring;

import com.progmatic.prisonerdilemmaspring.implementations.TitForTatGuy;
import com.progmatic.prisonerdilemmaspring.implementations.HonestGuy;
import com.progmatic.prisonerdilemmaspring.implementations.BadGuy;
import com.progmatic.prisonerdilemmaspring.implementations.PrejudicedGuy;
import com.progmatic.prisonerdilemmaspring.implementations.StrictTitForTatGuy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * @author peti
 */


public class PrisonerMain {

    public static final int PUNISHMENT_IF_BOTH_COOPERATES = 1;
    public static final int PUNISHMENT_IF_BOTH_BETRAYS = 2;
    public static final int PUNISHMENT_FOR_COOPERATOR_IF_ONE_COOPERATES = 3;
    public static final int PUNISHMENT_FOR_BETRAYER_IF_ONE_COOPERATES = 0;

    public static void main(String[] args) {
        List<Prisoner> prisoners = new ArrayList<>();
        //prisoners.add(new HonestGuy());
        prisoners.add(new BadGuy());
        prisoners.add(new BadGuy());
        prisoners.add(new TitForTatGuy());
        prisoners.add(new TitForTatGuy());
        prisoners.add(new StrictTitForTatGuy());
        prisoners.add(new StrictTitForTatGuy());
        prisoners.add(new PrejudicedGuy());
        prisoners.add(new PrejudicedGuy());
        PrisonerMain pm = new PrisonerMain(prisoners);
        pm.playGames(100);
        pm.printStatistics();
    }

    List<Prisoner> prisoners = new ArrayList<>();

    public PrisonerMain(List<Prisoner> prisoners) {
        this.prisoners = prisoners;       
    }

    public void playGames(int num) {
        for (int i = 0; i < num; i++) {
            playOneGame();
        }
    }

    public void playOneGame() {
        Collections.shuffle(prisoners);
        for (int i = 0; i < prisoners.size() - 1; i++) {
            Prisoner p1 = prisoners.get(i);
            for (int j = i + 1; j < prisoners.size(); j++) {
                Prisoner p2 = prisoners.get(j);
                play(p1, p2);

            }

        }
    }

    private void play(Prisoner p1, Prisoner p2) {
        //System.out.println("");
        //System.out.println(p1.getId() + " goona play with " + p2.getId());
        p1.playWith(p2.getId());
        p2.playWith(p1.getId());
        boolean p1Betray = p1.betray();
        boolean p2Betray = p2.betray();
        //System.out.println(p1.getId() + " betrayed? " + p1Betray);
        //System.out.println(p2.getId() + " betrayed? " + p2Betray);
        if (p1Betray && p2Betray) {
            p1.sentenceTo(PUNISHMENT_IF_BOTH_BETRAYS);
            p2.sentenceTo(PUNISHMENT_IF_BOTH_BETRAYS);
        }
        if (p1Betray && !p2Betray) {
            p1.sentenceTo(PUNISHMENT_FOR_BETRAYER_IF_ONE_COOPERATES);
            p2.sentenceTo(PUNISHMENT_FOR_COOPERATOR_IF_ONE_COOPERATES);
        }
        if (!p1Betray && p2Betray) {
            p1.sentenceTo(PUNISHMENT_FOR_COOPERATOR_IF_ONE_COOPERATES);
            p2.sentenceTo(PUNISHMENT_FOR_BETRAYER_IF_ONE_COOPERATES);
        }
        if (!p1Betray && !p2Betray) {
            p1.sentenceTo(PUNISHMENT_IF_BOTH_COOPERATES);
            p2.sentenceTo(PUNISHMENT_IF_BOTH_COOPERATES);
        }
        //printStatistics();
    }

    public void printStatistics() {
        Map<String, Integer> typeMap = new HashMap<>();
        for (Prisoner prisoner : prisoners) {
            System.out.println(prisoner.getId() + " has " + prisoner.finalScore() + " score");
            String type = prisoner.getClass().getName();
            typeMap.putIfAbsent(type, 0);
            typeMap.put(type, typeMap.get(type) + prisoner.finalScore());
        }
        System.out.println("******************");
        for (Map.Entry<String, Integer> entry : typeMap.entrySet()) {
            System.out.println(entry.getKey() + " has score: " + entry.getValue());
        }
    }

}
