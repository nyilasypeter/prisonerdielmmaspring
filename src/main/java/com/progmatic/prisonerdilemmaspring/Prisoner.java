/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring;

/**
 *
 * @author peti
 */
public interface Prisoner {
    String getId();
    void playWith(String otherPrisonerId);
    boolean betray();
    void sentenceTo(int years);
    int finalScore();
    
}
