/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring.implementations;

import com.progmatic.prisonerdilemmaspring.AbstractPrisoner;
import java.util.List;

/**
 *
 * @author peti
 */
public class TitForTatGuy extends AbstractPrisoner{

    @Override
    public boolean betray() {
        List<Boolean> historyWithThisGuy = history.get(lastPrisonerToPlayWith);
        if(historyWithThisGuy == null){
            return COOPERATE;
        }
        Boolean lastTimeThisGuyBetrayed = historyWithThisGuy.get(historyWithThisGuy.size()-1);
        if(lastTimeThisGuyBetrayed){
            return BETRAYE;
        }
        else{
            return COOPERATE;
        }
    }
    
}
