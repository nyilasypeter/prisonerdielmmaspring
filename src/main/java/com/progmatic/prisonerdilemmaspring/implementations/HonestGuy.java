/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring.implementations;

import com.progmatic.prisonerdilemmaspring.AbstractPrisoner;

/**
 *
 * @author peti
 */
public class HonestGuy extends AbstractPrisoner{

    @Override
    public boolean betray() {
        return false;
    }
    
}
