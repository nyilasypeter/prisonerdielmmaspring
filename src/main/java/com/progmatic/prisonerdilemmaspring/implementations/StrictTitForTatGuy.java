/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring.implementations;

import com.progmatic.prisonerdilemmaspring.AbstractPrisoner;
import java.util.List;

/**
 *
 * @author peti
 */
public class StrictTitForTatGuy extends AbstractPrisoner{

    @Override
    public boolean betray() {
        List<Boolean> historyWithThisGuy = history.get(lastPrisonerToPlayWith);
        if(historyWithThisGuy == null){
            return COOPERATE;
        }
        Boolean hasEverBetrayed = false;
        for (Boolean betrayed : historyWithThisGuy) {
            if(betrayed){
                hasEverBetrayed = true;
            }
        }
        if(hasEverBetrayed){
            return BETRAYE;
        }
        else{
            return COOPERATE;
        }
    }
    
}
