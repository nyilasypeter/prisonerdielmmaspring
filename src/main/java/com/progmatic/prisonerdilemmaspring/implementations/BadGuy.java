/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring.implementations;

import com.progmatic.prisonerdilemmaspring.AbstractPrisoner;
import java.util.UUID;

/**
 *
 * @author peti
 */
public class BadGuy extends AbstractPrisoner{
    
    @Override
    public boolean betray() {
        return true;
    }
    
}
