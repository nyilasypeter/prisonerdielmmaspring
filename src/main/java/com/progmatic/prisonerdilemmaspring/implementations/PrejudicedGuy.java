/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring.implementations;

import com.progmatic.prisonerdilemmaspring.AbstractPrisoner;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author peti
 */
public class PrejudicedGuy extends AbstractPrisoner {

    @Override
    public boolean betray() {
        Collection<List<Boolean>> values = history.values();
        if(values == null || values.isEmpty()){
            return COOPERATE;
        }
        int numOfBetrayals = 0;
        int numOfHonests = 0;
        for (List<Boolean> value : values) {
            for (Boolean betrayed : value) {
                if(betrayed){
                    numOfBetrayals++;
                }
                else{
                    numOfHonests++;
                }
            }
        }
        if(numOfBetrayals > numOfHonests){
            return BETRAYE;
        }
        else{
            return COOPERATE;
        }
    }
    
}
