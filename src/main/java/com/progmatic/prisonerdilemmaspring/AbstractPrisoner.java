/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.prisonerdilemmaspring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author peti
 */
public abstract class AbstractPrisoner implements Prisoner {

    protected final static Boolean BETRAYE = Boolean.TRUE;
    protected final static Boolean COOPERATE = Boolean.FALSE;
    protected int score = 0;
    protected String id = UUID.randomUUID().toString();
    protected Map<String, List<Boolean>> history = new HashMap<>();
    protected String lastPrisonerToPlayWith;  
    

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + "_" + id;
    }

    @Override
    public void playWith(String otherPrisonerId) {
        lastPrisonerToPlayWith = otherPrisonerId;
    }

    @Override
    public void sentenceTo(int years) {
        score -= years;
        history.putIfAbsent(lastPrisonerToPlayWith, new ArrayList<>());
        if (years == PrisonerMain.PUNISHMENT_FOR_COOPERATOR_IF_ONE_COOPERATES 
                || years == PrisonerMain.PUNISHMENT_IF_BOTH_BETRAYS) {
            history.get(lastPrisonerToPlayWith).add(BETRAYE);
        } else {
            history.get(lastPrisonerToPlayWith).add(COOPERATE);
        }

    }

    @Override
    public int finalScore() {
        return score;
    }

}
